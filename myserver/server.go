package main


import (
	"encoding/json"
	"fmt"
	"github.com/djumanoff/amqp"
)

var cfg = amqp.Config{
	Host:     "localhost",
	Port:     5672,
	User:     "guest",
	Password: "guest",
	LogLevel: 5,
}

var serverCfg = amqp.ServerConfig{
	//RequestX:  "",
	//ResponseX: "",
}

type NameRequest struct {
	MyName string `json:"my_name"`
}

type NameResponse struct {
	Msg string `json:"msg"`
}


func main() {
	fmt.Println("Start")

	sess := amqp.NewSession(cfg)

	if err := sess.Connect(); err != nil {
		fmt.Println(err)
		return
	}
	defer sess.Close()

	server, err := sess.Server(serverCfg)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = server.Endpoint("request.get.hello", handler)
	if err != nil {
		fmt.Println(err)
		return
	}

	if err := server.Start(); err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("End")

}

func handler(m amqp.Message) *amqp.Message {

	var req NameRequest
	var resp NameResponse

	if err := json.Unmarshal(m.Body, &req); err != nil {
		resp.Msg = fmt.Sprintf("unable to unmarshall: %v", err.Error())
		respBytes, err := json.Marshal(resp)
		if err != nil {
			panic(err)
		}

		return &amqp.Message{Body: respBytes}
	}

	resp.Msg = fmt.Sprintf("Nice to meet you Miss/Mister %s", req.MyName)
	respBytes, err := json.Marshal(resp)
	if err != nil {
		panic(err)
	}

	return &amqp.Message{Body: respBytes}

}




